const router = require('express').Router()
    , path = require('path')
    , tempy = require('tempy')
    , XLSX = require('xlsx')
    , moment = require('moment')
    , Parser = require('../private/modules/Parser')
    , generate_error_report = require('../private/modules/error_report')

// Homepage
router.get('/', (req, res) => {
    res.render(__basedir + '/views/index', { page:'index' }) 
})

// File upload request.
router.post('/', (req, res) => {
    
    if(req.files.file) {

        // Parse uploaded file. 
        new Parser(req)
                
                // // Open file
                // var workbook = XLSX.readFile(fname)

                // // Set worksheet
                // var worksheet = workbook.Sheets[workbook.SheetNames[0]]

                /**
                 * File name
                 * console.log(path.basename(req.files.file.name, path.extname(req.files.file.name)))
                 */

                // 

                 // Data validating/fill, etc.
                // custom_parse.process(XLSX.utils.sheet_to_json(worksheet, {header:'A', defval:null})).then((status)=>{
                //     req.flash('success', 'Update succesful!'); res.redirect('/');
                // }).catch(err=>{
                //     generate_error_report.Template_OCUpload(err).then((file)=>{

                //         req.session.file = file
       
                //         req.flash('danger', 'Issues encountered, download report <a href=\'/download\' onclick="this.parentNode.parentNode.removeChild(this.parentNode)">here</a>.')
                //         res.redirect('/')
                
                //     }).catch((err)=>console.log(err))
                // })


//        } else { req.flash('danger', 'File must be an excel file.'); res.redirect('/'); }     
    } else { req.flash('danger', 'Form was empty.'); res.redirect('/'); }
    
    
})

router.get('/download', (req, res) => {
    if(req.session.file) {
        res.download(req.session.file)
        req.session.file = null
    } else {
       req.flash('danger', 'No file.')
       res.redirect('/')
    }
})


router.get('/documentation', (req, res) => {
    res.render(__basedir + '/views/documentation', { page:'documentation' }) 
})


// 404
router.get('*', (req, res) => {
    res.status(404).render(__basedir + '/views/404', { page:'404' })
})

module.exports = router