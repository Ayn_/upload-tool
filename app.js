const express = require('express')
    , fileupload = require('express-fileupload')
    , compression = require('compression')
    , path = require('path')
    , routes = require('./routes/init')
    , keys = require('./config/keys')
    , cookieSession = require('cookie-session')    
    , flash = require('express-flash')

global.__basedir = __dirname

express()
.use(fileupload())
.use(compression())
.set('view engine', 'ejs')
.use(express.static(path.join(__dirname, 'public')))
.use(cookieSession({
    maxAge: keys.session.cookieAge,
    keys: [keys.session.cookieKey]
}))
.use(flash())
.use('/', routes)
.listen(80, () => console.log('App started'))