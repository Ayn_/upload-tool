const XLSX = require('xlsx')
    , tempy = require('tempy')
    , moment = require('moment')

// Template_OCUpload - Error report
module.exports = (data) => {

    return new Promise((resolve, reject)=>{
        if(data) {

            var wb = XLSX.utils.book_new()

            var ws = XLSX.utils.aoa_to_sheet([
                ["Errors"]
            ], {bold:true})
            
            data.forEach((error_set)=>{
                if(typeof error_set == 'string') {
                    XLSX.utils.sheet_add_aoa(ws, [
                        [error_set]
                    ], {origin: -1})
                } else {
                    Array.prototype.forEach.call(error_set, (row)=>{
                        XLSX.utils.sheet_add_aoa(ws, [
                            [row]
                        ], {origin: -1})
                    })
                }
            }) 

            XLSX.utils.book_append_sheet(wb, ws, 'test')

            var file = tempy.file({name: 'errors_'+moment().format('MM_DD_YYYY')+'.xlsx'});

            XLSX.writeFile(wb, file)

            resolve(file)

        } else { reject(false) }
    })

 
}
