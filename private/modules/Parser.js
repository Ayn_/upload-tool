const XLSX = require('xlsx')
    , tempy = require('tempy')    
    , path = require('path')
    , generate_error_report = require('./error_report')

class Parser {

    constructor(req) {

        // If excel move the uploaded file to sys temp file location. 
        if(path.extname(req.files.file.name) === '.xlsx' || '.xls') {

            var fname = tempy.file({extension: 'sheetjs'})

            req.files.file.mv(fname, (err) => {
                if (err)
                return res.status(500).send(err)

                // Open file
                var workbook = XLSX.readFile(fname)

                // Set worksheet
                var worksheet = workbook.Sheets[workbook.SheetNames[0]]

                var worksheetJson = XLSX.utils.sheet_to_json(worksheet, {header:'A', defval:null})

                var report

                // Determine report based on file name. 
                switch(req.files.file.name.split('.').slice(0, -1).join('.')) {
                    case 'Template_OCUpload':
                        report = new (require('./Reports/OCUpload'))(worksheetJson)
                        break;
                    case 'Template_OC_Device_Upload':
                        report = new (require('./Reports/OCDeviceUpload'))(worksheetJson)
                        break;
                    case 'Template_OCExternalIDUpdate':
                        report = new (require('./Reports/OCExternalIDUpdate'))(worksheetJson)
                        break;
                    case 'Template_SpecialistDataUpload':
                        report = new (require('./Reports/SpecialistDataUpload'))(worksheetJson);
                        break;
                    default: 
                        req.flash('danger', 'Invalid file used.'); req.res.redirect('/'); return;
                }


                report.then(()=> { req.flash('success', 'File has no issues!'); req.res.redirect('/'); }).catch(err=>{

                    generate_error_report(err).then((file)=>{
        
                        req.session.file = file
    
                        req.flash('danger', 'Issues encountered, download report <a href=\'/download\' onclick="this.parentNode.parentNode.removeChild(this.parentNode)">here</a>.')
                        req.res.redirect('/')
                
                    }).catch((err)=>console.log(err))

                }).catch((err)=>console.log(err))
            })
        } else { req.flash('danger', 'File must be an excel file.'); req.res.redirect('/'); }  
    
    }
}

module.exports = Parser