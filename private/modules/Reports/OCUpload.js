class OC_Upload {

    constructor(worksheetJson) {

        // Array for storing errors. 
        var failed_rows = []

        return new Promise((resolve, reject)=> {

            worksheetJson.reduce((done, row, index) =>
                
                done.then(()=> {
                    
                    var row_tracker = index+1 //Signifies excel rows

                    // First row - Check headers
                    if(row_tracker === 1) {
                        if(!this.valid_headers(row)) {
                            reject(['Invalid Excel Headers']) 
                        }
                    }
                    
                    // Headers are good - Validate data.
                    else {
                        var { errors } = this.validate_row(row, row_tracker)
                        
                        if(errors.length > 0) failed_rows.push(errors) 
                    }
                    
                }).catch(err=>console.log(err+'5.5'))
            , Promise.resolve()).then(()=>{if(failed_rows.length == 0) { console.log('Success!'); resolve(); } else { reject(failed_rows) }})
        })
    }

    // Method to validate file has correct headers. 
    valid_headers(row) {
        var errors = []  
    
        // Validate sheet.
        if(row.A != 'CriticalResultsID') { errors.push('Row A1 is not set to CriticalResultsID') }
        if(row.B != 'Active') { errors.push('Row B1 is not set to Active') }
        if(row.C != 'FirstName') { errors.push('Row C1 is not set to FirstName') }
        if(row.D != 'LastName') { errors.push('Row D1 is not set to LastName') }
        if(row.E != 'MiddleName') { errors.push('Row E1 is not set to MiddleName') }
        if(row.F != 'Email') { errors.push('Row F1 is not set to Email') }
        if(row.G != 'NationalProviderID') { errors.push('Row G1 is not set to NationalProviderID') }
        if(row.H != 'OfficePhone') { errors.push('Row H1 is not set to OfficePhone') }
        if(row.I != 'ReceiveMessageAs') { errors.push('Row I1 is not set to ReceiveMessageAs') }
        if(row.J != 'EnableMobile') { errors.push('Row J1 is not set to EnableMobile') }
        if(row.K != 'Address1') { errors.push('Row K1 is not set to Address1') }
        if(row.L != 'Address2') { errors.push('Row L1 is not set to Address2') }
        if(row.M != 'Address3') { errors.push('Row M1 is not set to Address3') }
        if(row.N != 'City') { errors.push('Row N1 is not set to City') }
        if(row.O != 'State') { errors.push('Row O1 is not set to State') }
        if(row.P != 'Zip') { errors.push('Row P1 is not set to Zip') }
        if(row.Q != 'Fax') { errors.push('Row Q1 is not set to Fax') }
        if(row.R != 'CellPhone') { errors.push('Row R1 is not set to CellPhone') }
        if(row.S != 'Specialty') { errors.push('Row S1 is not set to Specialty') }
        if(row.T != 'PracticeGroup') { errors.push('Row T1 is not set to PracticeGroup') }
        if(row.U != 'HospitalAffiliation') { errors.push('Row U1 is not set to HospitalAffiliation') }
        if(row.V != 'AdditionalContactName') { errors.push('Row V1 is not set to AdditionalContactName') }
        if(row.W != 'AdditionalContactPhone') { errors.push('Row W1 is not set to AdditionalContactPhone') }
        if(row.X != 'Pager') { errors.push('Row X1 is not set to Pager') }
        if(row.Y != 'RadiologyTDR') { errors.push('Row Y1 is not set to RadiologyTDR') }
        if(row.Z != 'LaboratoryTDR') { errors.push('Row Z1 is not set to LaboratoryTDR') }
        if(row.AA != 'ProfileReturned') { errors.push('Row AA1 is not set to ProfileReturned') }
        if(row.AB != 'ApplicationName') { errors.push('Row AB1 is not set to ApplicationName') }
        if(row.AC != 'ExternalId') { errors.push('Row AC1 is not set to ExternalId') }
        if(row.AD != 'ExternalIDType') { errors.push('Row AD1 is not set to ExternalIDType') }
        if(row.AE != 'HospitalFacilityId') { errors.push('Row AE1 is not set to HospitalFacilityId') }
    
        if(errors.length) return false; else return true 
    }
    
    // Self explanatory 
    validate_row(row, row_tracker) {
        var errors = [] 
        
        if(Object.values(row).some((value)=>value!=null))
          {
            // CriticalResultsID
            if(row.A) { if(!/^[0-9]{1,9}$/.test(row.A)) { errors.push('Row A' +row_tracker+ ' (Critical Result ID) must be a whole number with a maximun length of 9') } }
            
            // Active
            if(row.B) { if(!/^[0-1]{1}$/.test(row.B)) { errors.push('Row B' +row_tracker+ ' (Active) must be a whole number between 0 and 1') } }
            
            // FirstName
            if(row.C) { if(!/^[ a-zA-Z\,\-\.]{1,50}$/.test(row.C)) { errors.push('Row C' +row_tracker+ ' (Firstname) must not exceed 50 characters and can only contain the following characters: A-Z, a-z, comma, hyphen, period and space') } }
                else { errors.push('Row C' +row_tracker+ ' (Firstname) is mandatory') }

            // LastName
            if(row.D) { if(!/^[ a-zA-Z\,\-\.]{1,50}$/.test(row.D)) { errors.push('Row D' +row_tracker+ ' (Lastname) must not exceed 50 characters and can only contain the following characters: A-Z, a-z, comma, hyphen, period and space') } }
                else { errors.push('Row D' +row_tracker+ ' (Lastname) is mandatory') }
            
            // MiddleName
            if(row.E) { if(!/^[ a-zA-Z\,\-\.]{1,50}$/.test(row.E)) { errors.push('Row E' +row_tracker+ ' (Middlename) must not exceed 50 characters and can only contain the following characters: A-Z, a-z, comma, hyphen, period and space') } }
        
            // Email
            if(row.F) { if(!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(row.F)) { errors.push('Row F' +row_tracker+ ' (Email) must be a valid email address') } }
                else { errors.push('Row F' +row_tracker+ ' (Email) is mandatory') }
        
            // NationalProviderID
            if(row.G) { if(!/^[0-9]{10}$/.test(row.G)) { errors.push('Row G' +row_tracker+ ' (National Provider ID) must be a 10 digit number') } }
        
            // OfficePhone
            if(row.H) { row.H = row.H.replace(/[^\d]/g, ""); if(!/^[0-9]{10}$/.test(row.H)) { errors.push('Row H' +row_tracker+ ' (Office Phone) must be a 10 digit number') } }            
                else { errors.push('Row H' +row_tracker+ ' (Office Phone) is mandatory') }
        
            // ReceiveMessageAs
            if(row.I) { if(!/^[1-3]{1}$/.test(row.I)) { errors.push('Row I' +row_tracker+ ' (Recieve Messages As) must be a whole number between 1 and 3') } }
            
            // EnableMobile
            if(row.J) { if(!/^[0-1]{1}$/.test(row.J)) { errors.push('Row J' +row_tracker+ ' (Enable Mobile) must be a whole number between 0 and 1') } }            
            
            // Address1
            if(row.K) { if(!/^.{1,100}$/.test(row.K)) { errors.push('Row K' +row_tracker+ ' (Address 1) must not exceed 100 characters') } }
            
            // Address2
            if(row.L) { if(!/^.{1,100}$/.test(row.L)) { errors.push('Row L' +row_tracker+ ' (Address 2) must not exceed 100 characters') } }
            
            // Address3
            if(row.M) { if(!/^.{1,100}$/.test(row.M)) { errors.push('Row M' +row_tracker+ ' (Address 3) must not exceed 100 characters') } }
            
            // City
            if(row.N) { if(!/^[ a-zA-Z]{1,50}$/.test(row.N)) { errors.push('Row N' +row_tracker+ ' (City) must not exceed 50 characters') } }            
            
            // State
            if(row.O) { row.O = row.O.toUpperCase(); if(!/^[A-Z]{2}$/.test(row.O)) { errors.push('Row O' +row_tracker+ ' (State) must be 2 characters') } }
            
            // Zip
            if(row.P) { if(!/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(row.P)) { errors.push('Row P' +row_tracker+ ' (Zip) must be 5 digits or 5 digits-4 digits') } }
            
            // Fax
            if(row.Q) { row.Q = row.Q.replace(/[^\d]/g, ""); if(!/^[0-9]{10}$/.test(row.Q)) { errors.push('Row Q' +row_tracker+ ' (Fax) must be a 10 digit number') } }                        
                else { errors.push('Row Q' +row_tracker+ ' (Fax) is mandatory') }
            
            // CellPhone
            if(row.R) { row.R = row.R.replace(/[^\d]/g, ""); if(!/^[0-9]{10}$/.test(row.R)) { errors.push('Row R' +row_tracker+ ' (Cell Phone) must be a 10 digit number') } }                                    
            
            // Specialty
            if(row.S) { if(!/^[ a-zA-Z]{1,75}$/.test(row.S)) { errors.push('Row S' +row_tracker+ ' (Specialty) must not exceed 75 and can only contain the following characters: A-Z, a-z, 0-9 and space') } }            
            
            // PracticeGroup
            if(row.T) { if(!/^[ a-zA-Z]{1,100}$/.test(row.T)) { errors.push('Row T' +row_tracker+ ' (Practice Group) must not exceed 100 and can only contain the following characters: A-Z, a-z, 0-9 and space') } }            
            
            // HospitalAffiliation
            if(row.U) { if(!/^[ a-zA-Z]{1,100}$/.test(row.U)) { errors.push('Row U' +row_tracker+ ' (Hospital Affiliation) must not exceed 100 and can only contain the following characters: A-Z, a-z, 0-9 and space') } }            
            
            // AdditionalContactName
            if(row.V) { if(!/^[ a-zA-Z\,\-\.]{1,50}$/.test(row.V)) { errors.push('Row V' +row_tracker+ ' (Additional Contact Name) must not exceed 50 and can only contain the following characters: A-Z, a-z, comma, hyphen, period and space') } }
            
            // AdditionalContactPhone
            if(row.W) { row.W = row.W.replace(/[^\d]/g, ""); if(!/^[0-9]{10}$/.test(row.W)) { errors.push('Row W' +row_tracker+ ' (Additional Contact Phone) must be a 10 digit number') } }            
            
            // Pager
            if(row.X) { if(!/^[a-zA-Z]{1,100}$/.test(row.X)) { errors.push('Row X' +row_tracker+ ' (Pager) must not exceed 100 and can only contain the following characters: A-Z, a-z and 0-9') } }            
            
            // RadiologyTDR
            if(row.Y) { if(!/^[0-1]{1}$/.test(row.Y)) { errors.push('Row Y' +row_tracker+ ' (Radiology TDR) must be a whole number between 0 and 1') } }            
            
            // LaboratoryTDR
            if(row.Z) { if(!/^[0-1]{1}$/.test(row.Z)) { errors.push('Row Z' +row_tracker+ ' (Laboratory TDR) must be a whole number between 0 and 1') } }            
            
            // ProfileReturned
            if(row.AA) { if(!/^[0-1]{1}$/.test(row.AA)) { errors.push('Row AA' +row_tracker+ ' (Profile Returned) must be a whole number between 0 and 1') } }            
            
            // ApplicationName
            if(row.AB) { if(!/^.{1,100}$/.test(row.AB)) { errors.push('Row AB' +row_tracker+ ' (Application Name) must not exceed 100 characters') } }
            
            // ExternalId
            if(row.AC) { if(!/^.{1,100}$/.test(row.AC)) { errors.push('Row AC' +row_tracker+ ' (External ID) must not exceed 100 characters') } }
            
            // ExternalIDType
            if(row.AD) { if(!/^.{1,100}$/.test(row.AD)) { errors.push('Row AD' +row_tracker+ ' (External ID Type) must not exceed 100 characters') } }
            
            // HospitalFacilityId
            // if(row.AE) { errors.push('Row AE' +row_tracker+ ' is not set') }
          }
    
        return { errors }
    }  
}

module.exports = OC_Upload