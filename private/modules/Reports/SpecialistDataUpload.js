class SpecialistDataUpload {

    constructor(worksheetJson) {
        // Array for storing errors. 
        var failed_rows = []

        return new Promise((resolve, reject)=> {

            worksheetJson.reduce((done, row, index) =>
                
                done.then(()=> {
                    
                    var row_tracker = index+1 //Signifies excel rows

                    // First row - Check headers
                    if(row_tracker === 1) {
                        if(!this.valid_headers(row)) {
                            reject(['Invalid Excel Headers']) 
                        }
                    }
                    
                    // Headers are good - Validate data.
                    else {
                        var { errors } = this.validate_row(row, row_tracker)
                        
                        if(errors.length > 0) failed_rows.push(errors) 
                    }
                    
                }).catch(err=>console.log(err+'5.5'))
            , Promise.resolve()).then(()=>{if(failed_rows.length == 0) { console.log('Success!'); resolve(); } else { reject(failed_rows) }})
        })
    }

    // Method to validate file has correct headers. 
    valid_headers(row) {
        var errors = []  
    
        // Validate sheet.
        if(row.A != 'FirstName') { errors.push('Row A1 is not set to FirstName') }
        if(row.B != 'LastName') { errors.push('Row B1 is not set to LastName') }
        if(row.C != 'MiddleName') { errors.push('Row C1 is not set to MiddleName') }
        if(row.D != 'Email') { errors.push('Row D1 is not set to Email') }
        if(row.E != 'Phone') { errors.push('Row E1 is not set to Phone') }
        if(row.F != 'Fax') { errors.push('Row F1 is not set to Fax') }
        if(row.G != 'Specialty') { errors.push('Row G1 is not set to Specialty') }
        if(row.H != 'Affiliation') { errors.push('Row H1 is not set to Affiliation') }
        if(row.I != 'SendOverdueReportsByEmail') { errors.push('Row I1 is not set to SendOverdueReportsByEmail') }
        if(row.J != 'SendOverdueReportsByFax') { errors.push('Row J1 is not set to SendOverdueReportsByFax') }
        if(row.K != 'SendOverdueReportsOn') { errors.push('Row K1 is not set to SendOverdueReportsOn') }
        if(row.L != 'SendOverdueReportsAt') { errors.push('Row L1 is not set to SendOverdueReportsAt') }
        if(row.M != 'ShowClosedMessageSince') { errors.push('Row M1 is not set to ShowClosedMessageSince') }
    
        if(errors.length) return false; else return true 
    }
    
    // Self explanatory 
    validate_row(row, row_tracker) {
        var errors = [] 

        if(Object.values(row).some((value)=>value!=null)) {

            // FirstName
            if(row.A) { if(!/^[ a-zA-Z\,\-\.]{1,50}$/.test(row.A)) { errors.push('Row A' +row_tracker+ ' (Firstname) must not exceed 50 and can only contain the following characters: A-Z, a-z, comma, hyphen, period and space') } }
                else { errors.push('Row A' +row_tracker+ ' (Firstname) is mandatory') }
            
            // LastName
            if(row.B ) { if(!/^[ a-zA-Z\,\-\.]{1,50}$/.test(row.B)) { errors.push('Row B' +row_tracker+ ' (Lastname) must not exceed 50 and can only contain the following characters: A-Z, a-z, comma, hyphen, period and space') } }
                else { errors.push('Row B' +row_tracker+ ' (Lastname) is mandatory') }
            
            // MiddleName
            if(row.C) { if(!/^[ a-zA-Z\,\-\.]{1,50}$/.test(row.C)) { errors.push('Row C' +row_tracker+ ' (Middlename) must not exceed 50 and can only contain the following characters: A-Z, a-z, comma, hyphen, period and space') } }
            
            // Email
            if(row.D) { if(!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(row.D)) { errors.push('Row F' +row_tracker+ ' (Email) must be a valid email address') } }
            else { errors.push('Row D' +row_tracker+ ' (Email) is mandatory') }
    
            // Phone
            if(row.E) { row.E = row.E.replace(/[^\d]/g, ""); if(!/^[0-9]{10}$/.test(row.E)) { errors.push('Row E' +row_tracker+ ' (Office Phone) must be a 10 digit number') } }            
                else { errors.push('Row E' +row_tracker+ ' (Office Phone) is mandatory') }
        
            // Fax
            if(row.F) { row.F = row.F.replace(/[^\d]/g, ""); if(!/^[0-9]{10}$/.test(row.F)) { errors.push('Row F' +row_tracker+ ' (Fax) must be a 10 digit number') } }                        
            else { errors.push('Row F' +row_tracker+ ' (Fax) is mandatory') }

            // Specialty
            if(row.G) { if(!/^[ a-zA-Z]{1,75}$/.test(row.G)) { errors.push('Row G' +row_tracker+ ' (Specialty) must not exceed 75 and can only contain the following characters: A-Z, a-z, 0-9 and space') } }            

            // Affiliation
            if(row.H) { if(!/^[ a-zA-Z]{1,100}$/.test(row.H)) { errors.push('Row H' +row_tracker+ ' (Affiliation) must not exceed 100 and can only contain the following characters: A-Z, a-z, 0-9 and space') } }            

            // SendOverdueReportByEmail
            if(row.I) { if(!/^(?:Yes|No)$/i.test(row.I)) { errors.push('Row I' +row_tracker+ ' (SendOverdueReportByEmail) Must be "Yes" or "No" (Default value-No) - Currently case insensitive* ') } }            

            // SendOverdueReportByFax
            if(row.J) { if(!/^(?:Yes|No)$/i.test(row.J)) { errors.push('Row J' +row_tracker+ ' (SendOverdueReportByFax) Must be "Yes" or "No" (Default value-No) - Currently case insensitive* ') } }            

            // SendOverdueReportsOn
            if(row.K) { if(!/^(?:Sunday|Monday|Tuesday|Wednesday|Thursday|Friday|Saturday)$/i.test(row.K)) { errors.push('Row K' +row_tracker+ ' (SendOverdueReportsOn) Select which day or days to send a list of overdue report. Must be comma-Seprated days of the week. Example: Monday,Wednesday,Friday - Currently case insensitive* ') } }

            // SendOverdueReportsAt
            if(row.L) { if(!/^([01]\d|2[0-3]):?([0-5]\d)$/.test(row.L)) { errors.push('Row L' +row_tracker+ ' (SendOverdueReportsAt) Time of day to send overdue report list. Enter an hour on the 24-hour clock.') } }            

            // ShowClosedMessagesSince
            if(row.M) { if(!/^[1-9][0-9]?$|^30$/.test(row.M)) { errors.push('Row M' +row_tracker+ ' (ShowClosedMessagesSince) Number of days to look back for closed messages. Enter a number between 1 and 30(Default value- 5 days)') } }           

          } else { console.log('Empty row') }
    
        return { errors }
    }  
}

module.exports = SpecialistDataUpload