class OCDeviceUpload {

    constructor(worksheetJson) {
        console.log('parsing - OCDeviceUpload')
        // Array for storing errors. 
        var failed_rows = []

        return new Promise((resolve, reject)=> {
            console.log('File content - ', worksheetJson)

            worksheetJson.reduce((done, row, index) =>
                
                done.then(()=> {
                    
                    var row_tracker = index+1 //Signifies excel rows

                    console.log('current row ' + row_tracker)

                    // First row - Check headers
                    if(row_tracker === 1) {
                        if(!this.valid_headers(row)) {
                            reject(['Invalid Excel Headers']) 
                        }
                    }
                    
                    // Headers are good - Validate data.
                    else {
                        var { errors } = this.validate_row(row, row_tracker)
                        
                        if(errors.length > 0) failed_rows.push(errors) 
                    }
                    
                }).catch(err=>console.log(err+'5.5'))
            , Promise.resolve()).then(()=>{if(failed_rows.length == 0) { console.log('Success!'); resolve(); } else { reject(failed_rows) }})
        })
    }

    // Method to validate file has correct headers. 
    valid_headers(row) {
        var errors = []  
    
        // Validate sheet.
        if(row.A != 'NotificationDeviceName') { errors.push('Row A1 is not set to NotificationDeviceName') }
        if(row.B != 'CriticalResultsID') { errors.push('Row B1 is not set to CriticalResultsID') }
        if(row.C != 'FirstName') { errors.push('Row C1 is not set to FirstName') }
        if(row.D != 'LastName') { errors.push('Row D1 is not set to LastName') }
        if(row.E != 'DeviceType') { errors.push('Row E1 is not set to DeviceType') }
        if(row.F != 'DeviceAddress') { errors.push('Row F1 is not set to DeviceAddress') }
        if(row.G != 'Gateway') { errors.push('Row G1 is not set to Gateway') }
        if(row.H != 'Carrier') { errors.push('Row H1 is not set to Carrier') }
        if(row.I != 'FacilityID') { errors.push('Row I1 is not set to FacilityID') }
        if(row.J != 'GroupName') { errors.push('Row J1 is not set to GroupName') }
        if(row.K != 'FindingName') { errors.push('Row K1 is not set to FindingName') }
        if(row.L != 'NotificationEvent') { errors.push('Row L1 is not set to NotificationEvent') }
        if(row.M != 'Delete') { errors.push('Row M1 is not set to Delete') }
    
        if(errors.length) return false; else return true 
    }
    
    // Self explanatory 
    validate_row(row, row_tracker) {
        var errors = [] 
        
        if(Object.values(row).some((value)=>value!=null)) {

            // NotificationDeviceName
            if(row.A) { if(!/^[ a-zA-Z\,\-\.]{1,50}$/.test(row.A)) { errors.push('Row A' +row_tracker+ ' (NotificationDeviceName) must not exceed 50 characters and can only contain the following characters: A-Z, a-z, comma, hyphen, period and space') } }
            else { if(row.M == 'Yes') errors.push('Row A' +row_tracker+ ' (NotificationDeviceName) is mandatory since delete (Row M) is Yes') }

            // CriticalResultsID
            if(row.B) { if(!/^[0-9]{1,9}$/.test(row.B)) { errors.push('Row B' +row_tracker+ ' (Critical Result ID) must be a whole number with a maximun length of 9') } }

            // FirstName
            if(row.C) { if(!/^[ a-zA-Z\,\-\.]{1,50}$/.test(row.C)) { errors.push('Row C' +row_tracker+ ' (Firstname) must not exceed 50 characters and can only contain the following characters: A-Z, a-z, comma, hyphen, period and space') } }
                else { errors.push('Row C' +row_tracker+ ' (Firstname) is mandatory') }

            // LastName
            if(row.D) { if(!/^[ a-zA-Z\,\-\.]{1,50}$/.test(row.D)) { errors.push('Row D' +row_tracker+ ' (Lastname) must not exceed 50 characters and can only contain the following characters: A-Z, a-z, comma, hyphen, period and space') } }
                else { errors.push('Row D' +row_tracker+ ' (Lastname) is mandatory') }
            
            // DeviceType 
            if(row.E) { if(!/^(Email)|(Fax)|(SMS\(Cell\))|(SMS With Link)|(Pager\-Alpha)|(Pager\-Amcom)|(Pager\-Num\-PageUSA)|(Pager\-Num\-Regular)|(Pager\-Num\-SkyTel)|(Pager\-PartnersPaging)|(Pager\-TAP)|(Pager\-TAP\-Alpha)|(Vocera)|(OB\-Answering Service)|(OB\-Callback Instrutions)|(OB\-Callback Option)|(OB\-Results)$/.test(row.E)) { errors.push('Row E' +row_tracker+ ' (DeviceType) must match the values from the dropdown') } }
            else { errors.push('Row E' +row_tracker+ ' (DeviceType) is mandatory') }
            
            // DeviceAddress 
            if(row.F) { if(!/^[0-9]{1,50}$/.test(row.F)) { errors.push('Row F' +row_tracker+ ' (DeviceAddress) must be a whole number with a maximun length of 50') } }
            else { errors.push('Row F' +row_tracker+ ' (DeviceAddress) is mandatory') }

            // Gateway - Mandatory for some but not all.
            switch(row.E) {
                case 'SMS(Cell)':
                    errors.push('Row G' +row_tracker+ ' (Gateway) is mandatory for device type SMS(Cell) as set in column E'+row_tracker)
                    break;
                case 'Pager-Alpha':
                    errors.push('Row G' +row_tracker+ ' (Gateway) is mandatory for device type Pager-Alpha as set in column E'+row_tracker)
                    break;
            }

            // Carrier - Mandatory for some but not all
            switch(row.E) {
                case 'SMS(Cell)':
                    errors.push('Row H' +row_tracker+ ' (Carrier) is mandatory for device type SMS(Cell) as set in column E'+row_tracker)
                    break;
                case 'Pager-Alpha':
                    errors.push('Row H' +row_tracker+ ' (Carrier) is mandatory for device type Pager-Alpha as set in column E'+row_tracker)
                    break;
            }

            // FacilityID (Regex all would match regardless - Keeping it anyways)
            if(row.I) { if(!/^([a-zA-Z]{1,50})|(all)$/i.test(row.I)) { errors.push('Row I' +row_tracker+ ' (FacilityID) can use the word "All" in this field and make sure this is Facility ID not Facility name at institution level. Character allowed are A-Z, a-z, 0-9') } }
            else { errors.push('Row I' +row_tracker+ ' (FacilityID) is mandatory') }

            // GroupName
            if(row.J) { if(!/^([a-zA-Z]{1,50})|(all)$/i.test(row.J)) { errors.push('Row J' +row_tracker+ ' (GroupName) Can use the word "All" in this field. Group name define for institution. Character allowed are A-Z, a-z, 0-9') } }
            else { errors.push('Row J' +row_tracker+ ' (GroupName) is mandatory') }

            // FindingName
            if(row.K) { if(!/^([a-zA-Z]{1,50})|(all)$/i.test(row.K)) { errors.push('Row K' +row_tracker+ ' (FindingName) Can use the word "All" in this field. Group name define for institution. Character allowed are A-Z, a-z, 0-9') } }
            else { errors.push('Row K' +row_tracker+ ' (FindingName) is mandatory') }

            // NotificationEvent
            if(row.L) { if(!/^(Primary)|(Backup)|(Fail\-Safe)$/i.test(row.L)) { errors.push('Row L' +row_tracker+ ' (NotificationEvent) must be one of the following: Primary, Backup or Fail-Safe. (Case Insensitive)') } }            
            else { errors.push('Row L' +row_tracker+ ' (NotificationEvent) is mandatory') }

            // Delete
            if(row.M) { if(!/^(Yes)|(No)$/i.test(row.M)) { errors.push('Row M' +row_tracker+ ' (NotificationEvent) must be one of the following: Yes or No. (Case Insensitive)') } }            
    
        } else { console.log('Empty row') }
    
        return { errors }
    }  
}

module.exports = OCDeviceUpload