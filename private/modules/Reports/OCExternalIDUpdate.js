class OCExternalIDUpdate {

    constructor(worksheetJson) {
        console.log('parsing - OCExternalIDUpdate')
        // Array for storing errors. 
        var failed_rows = []

        return new Promise((resolve, reject)=> {
            console.log('File content - ', worksheetJson)

            worksheetJson.reduce((done, row, index) =>
                
                done.then(()=> {
                    
                    var row_tracker = index+1 //Signifies excel rows

                    console.log('current row ' + row_tracker)

                    // First row - Check headers
                    if(row_tracker === 1) {
                        if(!this.valid_headers(row)) {
                            reject(['Invalid Excel Headers']) 
                        }
                    }
                    
                    // Headers are good - Validate data.
                    else {
                        var { errors } = this.validate_row(row, row_tracker)
                        
                        if(errors.length > 0) failed_rows.push(errors) 
                    }
                    
                }).catch(err=>console.log(err+'5.5'))
            , Promise.resolve()).then(()=>{if(failed_rows.length == 0) { console.log('Success!'); resolve(); } else { reject(failed_rows) }})
        })
    }

    // Method to validate file has correct headers. 
    valid_headers(row) {
        var errors = []  
    
        // Validate sheet.
        if(row.A != 'CriticalResultsID') { errors.push('Row A1 is not set to CriticalResultsID') }
        if(row.B != 'FirstName') { errors.push('Row B1 is not set to FirstName') }
        if(row.C != 'LastName') { errors.push('Row C1 is not set to LastName') }
        if(row.D != 'FacilityID') { errors.push('Row D1 is not set to FacilityID') }
        if(row.E != 'ApplicationName') { errors.push('Row E1 is not set to ApplicationName') }
        if(row.F != 'ExternalId') { errors.push('Row F1 is not set to ExternalId') }
        if(row.G != 'ExternalIDType') { errors.push('Row G1 is not set to ExternalIDType') }
        if(row.H != 'Notes') { errors.push('Row H1 is not set to Notes') }
        if(row.I != 'IsDelete') { errors.push('Row I1 is not set to IsDelete') }
    
        if(errors.length) return false; else return true 
    }
    
    // Self explanatory 
    validate_row(row, row_tracker) {
        var errors = [] 
        
        if(Object.values(row).some((value)=>value!=null)) {

            // CriticalResultsID
            if(row.A) { if(!/^[0-9]{1,9}$/.test(row.A)) { errors.push('Row A' +row_tracker+ ' (Critical Result ID) must be a whole number with a maximun length of 9') } }
                else { errors.push('Row A' +row_tracker+ ' (CriticalResultsID) is mandatory') }

            // FirstName
            if(row.B) { if(!/^[ a-zA-Z\,\-\.]{1,50}$/.test(row.B)) { errors.push('Row B' +row_tracker+ ' (Firstname) must not exceed 50 characters and can only contain the following characters: A-Z, a-z, comma, hyphen, period and space') } }
                else { errors.push('Row B' +row_tracker+ ' (Firstname) is mandatory') }

            // LastName
            if(row.C ) { if(!/^[ a-zA-Z\,\-\.]{1,50}$/.test(row.C)) { errors.push('Row C' +row_tracker+ ' (Lastname) must not exceed 50 characters and can only contain the following characters: A-Z, a-z, comma, hyphen, period and space') } }
                else { errors.push('Row C' +row_tracker+ ' (Lastname) is mandatory') }
            
            // FacilityID (Regex all would match regardless - Keeping it anyways)
            if(row.D) { if(!/^([a-zA-Z]{1,50})|(all)$/i.test(row.D)) { errors.push('Row D' +row_tracker+ ' (FacilityID) must not exceed 50 characters') } }

            // ApplicationName 
            // Unable to verify this without DB access - Name of the External application (as found in Institution -> Manage Application)

            // ExternalID 
            // Unable to verify without DB Access - Value with ExternalIDType combination must not get duplicated with in file or in the system.

            // ExternalID Type
            // Unable to verify without DB Access - ID type of the external application exa : HIS ID,LIS ID etc.

            // Notes
            if(row.H) { if(!/^[ a-zA-Z0-9\,\-\.]{1,250}$/.test(row.H)) { errors.push('Row H' +row_tracker+ ' (Notes) must not exceed 250 characters and can only contain the following characters: A-Z, a-z, 0-9, commans, hyphen, periods and space') } }

            // IsDelete
            if(row.I) { if(!/^(1)|(Yes)|(True)|(0)|(No)|(False)$/i.test(row.I)) { errors.push('Row I' +row_tracker+ ' (IsDelete) must be one of the following: 1, Yes, True, 0, No, False. (Case Insensitive)') } }            

        } else { console.log('Empty row') }
    
        return { errors }
    }  
}

module.exports = OCExternalIDUpdate