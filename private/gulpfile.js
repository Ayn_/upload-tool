//npm install --save-dev gulp gulp-imagemin gulp-uglifyes gulp-sass gulp-concat gulp-clean-css gulp-connect
const gulp = require('gulp')
    , imagemin = require('gulp-imagemin')
    , uglify = require('gulp-uglifyes')
    , sass = require('gulp-sass')
    , concat = require('gulp-concat')
    , cleanCSS = require('gulp-clean-css')
    , connect = require('gulp-connect')
    , gutil = require('gulp-util')

/*
-- TOP LEVEL FUNCTIONS --
gulp.task - Define Tasks.
gulp.src - where to get the files.
gulp.dest - where to put the files.
gulp.watch - Watch files and folders for changes.
*/

// grunt defualt - runs when 'gulp' command is ran without calling a gulp method.
gulp.task('default', () => {
  return console.log('Gulp is running...')
})

// grunt defualt - can run other gulp methods if defined as an array.
gulp.task('default', ['message', 'imageMin', 'minify-sass', 'scripts', 'connect', 'watch'])

// Logs Message.
gulp.task('message', () => {
  return console.log('Gulp is running...')
})

// Live reload browser (used with chrome extension)
gulp.task('connect', function() {
  connect.server({
    root: '../',
    livereload: true
  })
})

// Optimize Images
gulp.task('imageMin', () => {
  gulp.src('images/*')
    .pipe(imagemin())
    .pipe(gulp.dest('../public/images'))
    .pipe(connect.reload())
})

//Sass => CSS => Minify
gulp.task('minify-sass', () => {
  gulp.src('stylesheets/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(concat('style.css'))
    .pipe(gulp.dest('../public/stylesheets'))
    .pipe(connect.reload())
})

//Minify and Combile all js - COMbine and conCATinate.
gulp.task('scripts', () => {
  gulp.src('javascript/*.js')
    .pipe(concat('main.js'))
    .pipe(uglify())
    .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
    .pipe(gulp.dest('../public/javascript'))
    .pipe(connect.reload())
})

//Watch
gulp.task('watch', () => {
  //gulp.watch('../views/*.*', ['reload'])
  gulp.watch('javascript/*.js', ['scripts'])
  gulp.watch('images/*', ['imageMin'])
  gulp.watch('stylesheets/*.scss', ['minify-sass'])
})
